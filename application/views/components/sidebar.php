<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="" class="brand-link">
    <img src="<?= base_url().'assets/dist/img/AdminLTELogo.png' ?>"
         alt="AdminLTE Logo"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">
    <span class="brand-text font-weight-light">Admin LTE</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <!-- <div class="image">
        <img src="<?= base_url().'assets/dist/img/user2-160x160.jpg' ?>" class="img-circle elevation-2" alt="User Image">
      </div> -->
      <div class="info">
        <!-- <p class="d-block">$user['nama']</p> -->
        <p class="d-block text-white">Dwiansyah Rhamadon</p>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a href="<?= base_url().'user/' ?>" class="nav-link">
            <i class="nav-icon fas fa-tachometer-alt"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>
