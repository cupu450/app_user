<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class User extends RestController {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('M_data_user', 'user');
    }

    public function index_get()
    {
       $id = $this->get('id');

        if ($id == '') {
            $data = $this->user->get_data('user',NULL,'Id');

            $res['status'] = 200;
            $res['message'] = 'berhasil mengambil data';
            $res['data'] = $data;

        } else {
          $data = $this->user->get_data('user',$id,'Id');

          if ($data) {
            $res['status'] = 200;
            $res['message'] = 'berhasil mengambil data';
            $res['data'] = $data;
          } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal mengambil data karena id tidak ditemukan';
          }
        }

        $this->response($res, $res['status']);
    }

    function index_post()
    {
        $data = $this->post();

        $insert = $this->user->post_data($data, 'user');

        if ($insert > 0) {
            $res['status'] = 200;
            $res['message'] = 'Sukses menambah data';
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal menambah data';
        }

        $this->response($res, $res['status']);
    }

    function index_put()
    {

      $data = [
        'nama'      => $this->put('nama'),
        'email'     => $this->put('email'),
        'password'  => $this->put('password'),
        'role'      => $this->put('role')
      ];

      $parameter = array('Id' => $this->put('id_user'));
      $update = $this->user->update_data($data, $parameter, 'user');

      if ($update > 0) {
          $res['status'] = 200;
          $res['message'] = 'Sukses mengubah data';
      } else {
          $res['status'] = 400;
          $res['message'] = 'Gagal mengubah data';
      }

      $this->response($res, $res['status']);
    }

    function index_delete()
    {
        $id = $this->delete('id');
        $result = $this->user->del_data('user', 'Id', $id);

        if ($result > 0) {
            $res['status'] = 200;
            $res['message'] = 'Sukses menghapus data';
        } else {
            $res['status'] = 400;
            $res['message'] = 'Gagal menghapus data';
        }

        $this->response($res, $res['status']);
    }

    function filter_data_post()
    {
        $tanggal = array(
            'start_date' => date('Y-m-d', strtotime($this->post('start_date'))),
            'end_date' => date('Y-m-d', strtotime($this->post('end_date')))
        );

        $data = $this->user->get_data_filter('user', $tanggal, $this->post('role'), $this->post('sortir'));

        $res['status'] = 200;
        $res['message'] = 'berhasil mengambil data';
        $res['data'] = $data;

        $this->response($res, $res['status']);
    }

}
