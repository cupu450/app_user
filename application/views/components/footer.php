<div class="modal fade" id="modal-lg">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ubah Password</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" id="changePass">
        <div class="modal-body">
          <div class="form-group">
            <label>Password lama</label>
            <input type="Password" name="password_lama" class="form-control" placeholder="Masukan Password Lama" required>
          </div>
          <div class="form-group">
            <label>Password Baru</label>
            <input type="Password" name="password_baru" class="form-control" placeholder="Masukan Password Baru" required>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

<footer class="main-footer">
  <div class="float-right d-none d-sm-block">
    <b>Version</b> 3.0.5
  </div>
  <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">Badan POM Palembang</a>.</strong> All rights
  reserved.
</footer>
