$(document).ready(function(){
  
      $(window).on('load', function() {
        $.ajax({
          url: BaseUrl+'user/first_data',
          method:"GET",
          success:function(result){
            var data = JSON.parse(result);

            // console.log(data.data);
            var html = '';
            var i;
            for(i=0; i<data.data.length; i++){
                html += '<tr>'+
                        '<td>'+data.data[i].nama+'</td>'+
                        '<td>'+data.data[i].email+'</td>'+
                        '<td>'+data.data[i].role+'</td>'+
                        '<td>'+data.data[i].created_at+'</td>'+
                        '<td>'+
                          '<button class="btn btn-primary btn-flat upd-user" data-id="'+data.data[i].Id+'" data-nama="'+data.data[i].nama+'" data-email="'+data.data[i].email+'" data-role="'+data.data[i].role+'" data-pass="'+data.data[i].password+'" ><span class="fa fa-edit"></span></button> '+
                          '<button class="btn btn-danger btn-flat" onClick="del_user('+data.data[i].Id+');"><span class="fa fa-trash"></span></button>'+
                        '</td>'+
                        '</tr>';
            }
            $('#show_data').html(html);
            
          }
        })
      });

      $(document).on("click", ".upd-user", function(){
        $('#modal-upd').modal('show');
        $('[name="id_user"]').val($(this).attr('data-id'));
        $('[name="nama_upd"]').val($(this).attr('data-nama'));
        $('[name="email_upd"]').val($(this).attr('data-email'));
        $('[name="password_upd"]').val($(this).attr('data-pass'));
        $('[name="role_upd"]').val($(this).attr('data-role'));
      });
});

function del_user(id)
{
  Swal.fire({
    title: 'Apakah kamu yakin?',
    text: "Data akan dihapus permanen!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Iya',
    cancelButtonText: 'Tidak',
    allowOutsideClick: false
  }).then((result) => {
    if (result.value) {
      $.ajax({
         url: BaseUrl+'user/del_user_process',
         method:"POST",
         data:{'id':id},
         success:function(result){
           var data = JSON.parse(result);

           Swal.fire({
            title: 'Terhapus',
            text: data.message,
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'Oke',
            allowOutsideClick: false
          }).then((result) => {
            if (result.value) {
              location.reload();
            }
          })
         }
      })
    }
  })
}