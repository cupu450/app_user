<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data_user extends CI_Model {

    public function get_data($tabel,$id = null, $param = null)
    {
      if ($id == '') {
          if ($param != null) {
            $this->db->order_by($param,'desc');
          }

          $data = $this->db->get($tabel);
      } else {
          $this->db->where($param, $id);
          $data = $this->db->get($tabel);
      }

  		return $data->result();
    }

    public function post_data($data, $tabel)
    {
      $this->db->set($data);
      $this->db->insert($tabel);
      return $this->db->affected_rows();
    }

    public function update_data($data, $where, $tabel)
    {
      $this->db->where($where);
      $this->db->update($tabel,$data);
      return $this->db->affected_rows();
    }

    public function del_data($tabel, $param, $id)
    {
      $this->db->where($param, $id);
      $this->db->delete($tabel);
      return $this->db->affected_rows();
    }

    function get_data_filter($tabel, $tanggal, $role, $sortir)
    {
        if (empty($role)) {
            $condition = "created_at BETWEEN " . "'" . $tanggal['start_date'] . "'" . " AND " . "'" . $tanggal['end_date'] . "'";
        } elseif (count($role) == 1) {
            $condition = "created_at BETWEEN '".$tanggal['start_date']."' AND '".$tanggal['end_date']."' AND role = '".$role[0]."' ";
        } elseif (count($role) == 2) {
            $condition = "created_at BETWEEN '".$tanggal['start_date']."' AND '".$tanggal['end_date']."' AND role = '".$role[0]."' OR role = '".$role[1]."' ";
        } elseif (count($role) == 3) {
            $condition = "created_at BETWEEN '".$tanggal['start_date']."' AND '".$tanggal['end_date']."' AND role = '".$role[0]."' OR role = '".$role[1]."' OR role = '".$role[2]."' ";
        }
        
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where($condition);
        $this->db->order_by('nama',$sortir);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

}
