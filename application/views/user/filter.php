<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1>User Page</h1>
      </div>
    </div>
  </div>
</section>

<section class="content">
  <div class="card">
    <form id="search_data" method="post">
        <div class="card-header">
        <h3 class="card-title">Data Filter</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div clas="form-group">
                        <label>Start Date</label>
                        <input type="date" class="form-control" name="start_date" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div clas="form-group">
                        <label>End Date</label>
                        <input type="date" class="form-control mb-2" name="end_date" required>
                        <!-- <p class="text-sm mb-0">Saat memilih tanggal yang kedua harus memilih tanggal besoknya contoh:
                        saya memilih tanggal hari ini yaitu tanggal 16 agustus 2020 maka di form end date saya memilih
                        tanggal 17 agustus 2020 atau 18 atau 19 atau 20 dst. karena jika tidak seperti itu maka
                        data pada tanggal 16 agustus tidak akan muncul walaupun kita pilih 16 agustus
                        </p> -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mt-3">
                        <label>Pilih Role</label>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" name="role[]" id="cbx_user" type="checkbox" value="User">
                            <label for="cbx_user" class="custom-control-label">User</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" name="role[]" id="cbx_adm" type="checkbox" value="Admin">
                            <label for="cbx_adm" class="custom-control-label">Admin</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" name="role[]" id="cbx_sa" type="checkbox" value="Super Admin">
                            <label for="cbx_sa" class="custom-control-label">Super Admin</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group mt-3">
                        <label>Sorting Nama</label>
                        <select name="sorting" class="form-control" required>
                            <option value="" selected disabled>Pilih Jenis Sorting</option>
                            <option value="ASC">Ascending</option>
                            <option value="DESC">Descending</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary btn-block">Search Data</button>
        </div>
    </form>
  </div>
</section>

<section class="content">
  <div class="card">
    <div class="card-header">
        <h3 class="card-title">Data User</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fas fa-times"></i></button>
        </div>
    </div>
    <div class="card-body">
    <button class="btn btn-primary mb-3" data-toggle="modal" data-target="#modal-add"><span class="fa fa-plus"></span> Tambah User</button>
        <div class="table-responsive">
            <table class="table table-bordered" id="tabel-userku">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Created Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="show_data">
                    
                </tbody>
                <tbody id="show_data_filter">
                
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        
    </div>
  </div>
</section>

<div class="modal fade" id="modal-add">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" id="add_user">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <input class="form-control" name="nama" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role" class="form-control" required>
                            <option value="" selected disabled>Pilih Option Role</option>
                            <option>User</option>
                            <option>Admin</option>
                            <option>Super Admin</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>

    </div>
  </div>
</div>

<div class="modal fade" id="modal-upd">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="post" id="edit_user">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="hidden" name="id_user" required>
                        <input class="form-control" name="nama_upd" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email_upd" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password_upd" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role_upd" class="form-control" required>
                            <option value="" selected disabled>Pilih Option Role</option>
                            <option>User</option>
                            <option>Admin</option>
                            <option>Super Admin</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      </form>

    </div>
  </div>
</div>