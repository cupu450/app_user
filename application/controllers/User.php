<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use GuzzleHttp\Client;

class User extends CI_Controller {

	public function index()
	{
        $client = new GuzzleHttp\Client();
        $data['judul'] = 'Kompis | Data User';
        
        $this->template->display('user/filter', $data);
    }
    
    public function add_user_process()
    {
        $client = new GuzzleHttp\Client();
        $result = $client->post(base_url().'api_server/index.php/data_user', [
            'form_params' => [
			    'nama'      => $this->input->post('nama'),
                'email' 	=> $this->input->post('email'),
                'password'  => $this->input->post('password'),
                'role'      => $this->input->post('role')
            ]
      ]);
      echo $result->getBody();
    }

    public function del_user_process()
    {
        $client = new GuzzleHttp\Client();
        $result = $client->delete(base_url().'api_server/index.php/data_user', [
          'form_params' => [
            'id' => $this->input->post('id')
          ]
      ]);
      echo $result->getBody();
    }

    public function upd_user_process()
    {
        $client = new GuzzleHttp\Client();
        $result = $client->put(base_url().'api_server/index.php/data_user', [
            'form_params' => [
			    'nama'      => $this->input->post('nama_upd'),
                'email' 	=> $this->input->post('email_upd'),
                'password'  => $this->input->post('password_upd'),
                'role'      => $this->input->post('role_upd'),
                'id_user'   => $this->input->post('id_user')
            ]
      ]);
      
      echo $result->getBody();
    }

    public function search_user()
    {
      $client = new GuzzleHttp\Client();
        $result = $client->post(base_url().'api_server/index.php/search_data', [
            'form_params' => [
              'start_date' => $this->input->post('start_date'),
              'end_date' => $this->input->post('end_date'),
              'role' => $this->input->post('role'),
              'sortir' => $this->input->post('sorting')
            ]
      ]);

      echo $result->getBody();
    }

    public function first_data()
    {
      $client = new GuzzleHttp\Client();
      $result = $client->get(base_url().'api_server/index.php/data_user');

      echo $result->getBody();
    }
}
