
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $judul ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url().'assets/plugins/fontawesome-free/css/all.min.css' ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= base_url().'assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css' ?>">
  <link rel="stylesheet" href="<?= base_url().'assets/plugins/toastr/toastr.min.css' ?>">
  <link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
  <link rel="stylesheet" href="<?= base_url().'assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url().'assets/dist/css/adminlte.min.css' ?>">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="<?= base_url().'assets/dist/css/style.css' ?> ">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url().'assets/dist/img/apple-icon.png' ?>">
  <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url().'assets/dist/img/favicon1.png' ?>">

</head>
<body class="hold-transition sidebar-mini text-sm">
  <div id="myloading" style="display: none;"></div>
<!-- Site wrapper -->
<div class="wrapper">

  <!-- HEADER -->
  <?= $_header; ?>
  <!-- END HEADER -->

  <!-- SIDEBAR -->
  <?= $_sidebar ?>
  <!-- END SIDEBAR -->

  <div class="content-wrapper">
    <!-- CONTENT -->
    <?= $_content ?>
    <!-- END CONTENT -->
  </div>

  <!-- FOOTER -->
  <?= $_footer ?>
  <!-- END FOOTER -->

  <aside class="control-sidebar control-sidebar-dark"></aside>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script type="text/javascript">
    var BaseUrl = '<?= base_url() ?>';
</script>
<script src="<?= base_url().'assets/plugins/jquery/jquery.min.js' ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url().'assets/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/sweetalert2/sweetalert2.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/toastr/toastr.min.js' ?>"></script>
<!-- AdminLTE App -->
<script src="<?= base_url().'assets/dist/js/adminlte.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
<script src="<?= base_url().'assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js' ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url().'assets/dist/js/mine.js' ?>"></script>
<script src="<?= base_url().'assets/dist/js/demo.js' ?>"></script>
<script src="<?= base_url().'assets/dist/js/ajax.js' ?>"></script>
</body>
</html>
