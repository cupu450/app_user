$(document).ready(function(){

  $('#add_user').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/add_user_process',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(result){
         $('#myloading').fadeOut();
         var data = JSON.parse(result);

         Swal.fire({
           icon: 'success',
           title: data.message,
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  });

  $('#edit_user').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/upd_user_process',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(result){
         $('#myloading').fadeOut();
         var data = JSON.parse(result);

         Swal.fire({
           icon: 'success',
           title: data.message,
           showConfirmButton: true,
           allowOutsideClick: false
         }).then((result) => {
           if (result.value) {
             location.reload();
           }
         })
       }
    })
  });

  $('#search_data').on('submit', function(event){
    event.preventDefault();
    $.ajax({
       url: BaseUrl+'user/search_user',
       method:"POST",
       data:new FormData(this),
       contentType:false,
       cache:false,
       processData:false,
       beforeSend:function(){
          $('#myloading').fadeIn();
        },
       success:function(result){
        $('#show_data').remove();
        $('#myloading').fadeOut();

        var data = JSON.parse(result);
        // console.log(data.data.length);

        var html = '';
        var i;
        for(i=0; i<data.data.length; i++){
            html += '<tr>'+
                    '<td>'+data.data[i].nama+'</td>'+
                    '<td>'+data.data[i].email+'</td>'+
                    '<td>'+data.data[i].role+'</td>'+
                    '<td>'+data.data[i].created_at+'</td>'+
                    '<td>'+
                          '<button class="btn btn-primary btn-flat upd-user" data-id="'+data.data[i].Id+'" data-nama="'+data.data[i].nama+'" data-email="'+data.data[i].email+'" data-role="'+data.data[i].role+'" data-pass="'+data.data[i].password+'" ><span class="fa fa-edit"></span></button> '+
                          '<button class="btn btn-danger btn-flat" onClick="del_user('+data.data[i].Id+');"><span class="fa fa-trash"></span></button>'+
                        '</td>'+
                    '</tr>';
        }
        $('#show_data_filter').html(html);
         
       }
    })
  });

})
