<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$data['judul'] = 'Kompis | User';
		$this->template->display('welcome_message', $data);
	}
}
